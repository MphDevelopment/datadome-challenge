# Datadome-challenge

Théo JAMMES-BEUVE<br>
theo.jammes@gmail.com
___ 

## Step 1 : Data Analysis
To analyze the data, I used the scala spark-shell.

Before using any tool to analyze this log file, we can already see in the first lines that there are indeed bots that visit this website, "legit bots" if I can say so : crawlers that constantly visit the internet in order to maintain a valid list of what is available. This kind of bots should be respecting the website's *robots.txt* that defines what bots can interact with on the website.

However, let's hop into spark to work on the data, a file with the most important scala lines is available at [code.scala](./src/scala-analysis/code.scala).

### Using user-agents
First, we will detect bots that revealed their nature through their user-agent :<br>
![](./img/user-agent_check.png)

Now we have a list of "client" that were bots, eventhough they should all be obeying the *robots.txt* file, we check if they really did :<br>
![](./img/user-agent_robots.png)

Here we discover that two bots did not play by the rules and tried to access `/administrator` :
 - DiscordBot 2.0
 - intelx.io Bot

#### Filtering out these bots
Now that we have identified these bots, that (for the most part) played by the rules, we can remove them from our dataframe :<br>
![](./img/cleaning_df.png)

But when checking the size of this cleaned dataframe, we discover that the size doesn't match anymore, becaused we cleaned every lines that had their IP identified as a bot **at least once**. This could indicate someone was trying to create a bot and worked it on this website precisely (checking with his browser before running the bot) or someone was trying to create a hidden bot that would lie on its user-agent. However, I didn't really push further.<br>
![](./img/size_difference.png)

### Using the amount of visits
As scrapers tend to visit everything they can on a website, it might be a good idea to investigate clients with a high visit count.<br>
![](./img/visit_count.png)

However, after checking more carefully these IPs, they seem to be from the website administrators, which is actually not surprising in the end..

### Error 404 and absence of referrer
As web bots are automated and often do not interact directly with the website page, there is a high possibility that they only send request without any referrer.. Also crawling around a website could lead them to *virtual* dead ends, content that is not available outside from the host server. So I decided to check for these two information at the same time : <br>
![](./img/empty_referrer_404.png)

Here we observe one IP : *52.203.41.188* that combines a lot of 404 errors without referrer, with addition to a real fast delay between requests.. There's a possiblity that this is just a broken part of the website, and that these fast requests are just resources being loaded.. Given that this is the only activity coming from that IP, it might also be a bot..

### Good bots ? Bad bots ?
As I mentioned above, there are "good bots" accessing this website, such as the GoogleBot for instance, that request the robots.txt, and respect it while crawling the website. Its main purpose is to maintain a database of what is available on the internet and on websites.

However, we also noticed two "bad bots" that didn't respect the boundaries established by the website..
I didn't find any relevant information about DiscordBot 2.0, but the intelx.io Bot seems to be linked to the website of Intelligence X, which seems to be a company that offers a search engine that will try to scrap pretty much anything you would ask it to.

I will also take a shot at finding a disguised bot by using the assumption that *52.203.41.188* is indeed a bot. Trying a lot of paths that returned a 404 error code, if it was a bot it looks like it would be looking for possible vulnerability or simply trying to scrap every information from the website.

## Step 2 : Implementation
I will not offer a complete implementation of a bot detection system using the website accesses log, but only some pointers of how I visualize it.

The general idea I used was that there was a Java Object (*Subject*) that receives log from the website and notifies a BotDetection system.<br>
The BotDetectionSystem : [BotDetection.java](./src/java/BotDetection.java) would try to detect bots based on a couple attributes :
- The delay between requests
- The amount of 404 errors in the last requests
- And of course the user-agent and the respect of the robots.txt file.

You can find files at [src/java](./src/java)
