public class BotDetection implements Observer {
	
	/*
		Store data in the form of : 
		{IP : {InfoType : Info..} } 
		Useful information could be : Last access, last referrer, last request status + path, last user-agent ?, once used a bot user-agent, etc..
	*/
	Map<String, Information> ip2Info;
	
	
	/**
		Called everytime the subject receives new logs
	**/
	void update(subject Subject){
		List<Log> logs = subject.getState();
		this.analyze(logs);
	}
	
	private void analyze(List<Log> logs){
		Log log;
		do{
			log = logs.pop();
			this.checkLog(log);
			
		}while (log != null)
	}

	private void checkLog(log){
		Information info;
		//Retrieve or create information for the ip
		if(ip2Info.contains(log.getIp())){
			info = ip2info.get(log.getIp());
			info.update(log);			}
		} else
		{
			info = new InformationBuilder().fromLog(log);
			ip2info.put(log.getIp(), info);
		}
		
		
		//General idea of detection, here using if
		// but could be threaded to apply multiple detection types at the same time
		// it would probably allow for a more robust detection by coupling results
		
		
		//detect through userAgent
		if (info.usedBotUserAgent()){
			Logger.log(log.getId() + "was identified as a bot by its user-agent" + 
				info.respectedRobotsTxt() ? "" : " but didnt respect robots.txt.");
		}
		
		//Here it is important that the Information#Update method ignores 
		//request fired automatically by the website
		if (info.getAvgDelayBetweenRequests(10) < 1)
		{
			Logger.log(log.getId() + "is sending request with a fast delay, " +
				"and might be a bot";
		}
		
		if (info.status404Count(10) < 5){
			Logger.log(log.getId() + "is sending multiple request that result in a 404 error, " +
				"and might be a bot";
		}	
	}

	
}
