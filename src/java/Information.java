
interface Information{
	/**
	@return the last request's Date
	**/
	Date getLastDate();
	
	/**	
		@param requestAmount : the amount of request to compute average delay
		@return double the average delay between requests
	**/
	double getAvgDelayBetweenRequests(int requestAmount);
	
	/**
		@return true if a bot user-agent was used at least once
	**/
	boolean usedBotUserAgent();
	
	/**
		Updates class attributes given a Log
		
		This method should act carefuly towards requests fired automatically to
		retrieve resources in order to display the website.
		Hence only registering request fired by the user.
	**/
	void update(Log log);
	
	/**
		Return the last userAgent registered
	**/
	String lastUserAgent();
	
	/**
		@return int the total amount of 404 errors code
	**/
	int status404Count();
	
	/**
		@param requestAmount : the amount of request to compute average delay
		@return int the amount of 404 errors in the last requests
	**/
	int status404Count(int requestAmount);
	
	/**
		@return true if the robots.txt was respected
	**/
	boolean respectedRobotsTxt();
	
}
	
