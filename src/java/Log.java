
interface Log {
	
	String getIp();
	
	String getIdentd();
	
	String getUserId();
	
	Date getDate();
	
	String getReqMethod();
	
	String getReqPath();
	
	String getReqVersion();
	
	String getReqStatus();
	
	int getReqSize();
	
	String getReferrer();
	
	String getUserAgent();
	
}
