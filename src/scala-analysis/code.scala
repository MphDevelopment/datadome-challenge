import scala.io.Source
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import scala.util.matching.Regex

//We load the file in an Array[String]
val lines = Source.fromFile("access.log").getLines.toArray

/*
 I used a regex to properly separate information from the logs,
 there might be a better solution for it
*/
val regex = new Regex("""(\S+) (\S+) (\S+) \[(\S+) (\S+)\] \"(\S+) (\S+) (\S+) (\S+) (\S+) (\S+) (.*) (\S+)""", "ip", "identd", "userid", "date", "timezone", "reqMethod", "reqPath", "reqVersion", "status", "size", "referrer", "userAgent", "jsp")

//We separate attributes from the lines
val subgroups = lines.map(l => regex.findFirstMatchIn(l).get.subgroups)

//We create a list of Row in order to create our dataframe
var data : List[Row] = Nil

for(groups <- subgroups){
	data = data ::: Row.fromSeq(groups) :: Nil
}

//We define the schema of our dataframe
val schema = StructType(
				List(
					StructField("ip", StringType, true),
					StructField("identd", StringType, true),
					StructField("userid", StringType, true),
					StructField("date", StringType, true),
					StructField("timezone", StringType, true),
					StructField("reqMethod", StringType, true),
					StructField("reqPath", StringType, true),
					StructField("reqVersion", StringType, true),
					StructField("status", StringType, true),
					StructField("size", StringType, true),
					StructField("referrer", StringType, true),
					StructField("userAgent", StringType, true),
					StructField("jsp", StringType, true)
				)
			)

// Creation of the dataframe
val rdd = spark.sparkContext.parallelize(data)
val df = spark.createDataFrame(rdd, schema)

//creating another df with IPs that used bots : 
var usedBots = df.filter($"userAgent".contains("bot")

//check for bots that didn't respect robots.txt
var noRespect = usedBots.filter($"reqPath".contains("/administrator"))

//Checking for requests that returned a 404 and without any referrer
var suspicous = df.filter("status == '404' and referrer == '\"-\"')


